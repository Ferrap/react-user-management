import React from "react";
import PrimaryButton from "../primary-button/primary-button"
import MenuItem from "./menu-item/menu-item";
import "./menu.css";
import { withRouter  } from "react-router-dom";


const Menu = withRouter(({ history }) => {

    const itemClick = (path) => {
        history.push(path);
    }

    return (
        <div className="navbar">
            <h1 className="title">Gestion d'utilisateurs</h1>
            <div className="items">
                <MenuItem className="item" text="Liste des utilisateurs" onClick={() => itemClick("/users")}></MenuItem>
                <PrimaryButton className="item" text="Créer" color='#fc8755' onClick={() => itemClick("/user-creation")}></PrimaryButton>
            </div>
        </div>
    );
})

export default Menu;

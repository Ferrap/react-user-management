import React from "react";
import "./menu-item.css"

const MenuItem = ({ text, onClick }) => {
    return (
        <p className="item" onClick={ onClick }>{ text }</p>
    );
};

export default MenuItem
import './text-editor.css';

const TextEditor = ({text, placeholder, name, hasError, width, onChange}) => {

    return (
        <div className="text-editor" style={{width: width}}>
            <label className="label" htmlFor={name}>{text}</label>
            <input className="input" name={name} placeholder={placeholder} onChange={onChange}></input>
        </div>
    )

}

export default TextEditor;
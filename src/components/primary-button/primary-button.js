import "./primary-button.css";

const PrimaryButton = ({ text, onClick, color, width, type }) => {
    return (
        <button className="button" style={{backgroundColor: color, width: width ? width : 'fit-content'}} onClick={onClick}>{ text }</button>
    );
};

export default PrimaryButton;
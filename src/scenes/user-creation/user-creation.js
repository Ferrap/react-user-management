import React, { useState } from 'react';
import { connect } from 'react-redux'
import { addUser } from '../../services/users.service';
import PrimaryButton from '../../components/primary-button/primary-button';
import TextEditor from '../../components/text-editor/text-editor';
import './user-creation.css'

const mapDispatchToProps = (dispatch) => {
    return {
        addUser: user => dispatch(addUser(user))
    };
} 

const UserCreation = (props) => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [sexe, setSexe] = useState('male');
    const [birthday, setbirthday] = useState('');
    const [statut, setStatut] = useState('');
    const [adresse, setAdresse] = useState('');

    const submit = () => {
        const user = {
            id: new Date().getTime(),
            firstName,
            lastName,
            email,
            sexe,
            birthday,
            statut,
            adresse
        };
        props.addUser(user);
    }

    return (
        <div className="users-creation-content">
            <div className="user-form">
                <form className="form">
                    <h2 className="title-form">Informations sur l'utilisateur</h2>
                    <div className="dbl-fields">
                        <TextEditor name="lastName" text="Nom :" width="47%" onChange={(event) => setLastName(event.target.value)}></TextEditor>
                        <TextEditor name="firstName" text="Prénom :" width="47%" onChange={(event) => setFirstName(event.target.value)}></TextEditor>
                    </div>
                    <div className="field">
                        <TextEditor name="email" text="Email :" width="100%" onChange={(event) => setEmail(event.target.value)}></TextEditor>
                    </div>
                    <div className="genre">
                        <div onChange={(event) => setSexe(event.target.value)}>
                            <input type="radio" id="male" name="sexe" value="male" defaultChecked/>
                            <label htmlFor="male">Homme</label>
                        </div>
                        <div onChange={(event) => setSexe(event.target.value)}>
                            <input type="radio" id="female" name="sexe" value="female"/>
                            <label htmlFor="female">Femme</label>
                        </div>
                    </div>
                    <div className="dbl-fields">
                        <TextEditor name="birthDate" text="Date de naissance :" width="47%" placeholder="JJ/MM/YYYY" onChange={(event) => setbirthday(event.target.value)}></TextEditor>
                        <TextEditor name="statut" text="Statut :" width="47%" onChange={(event) => setStatut(event.target.value)}></TextEditor>
                    </div>
                    <div className="field">
                        <TextEditor name="adresse" text="Adresse :" width="100%" onChange={(event) => setAdresse(event.target.value)}></TextEditor>
                    </div>
                </form>
                <div className="btn-valider">
                    <PrimaryButton text="Valider" width="100%" color='#fc8755' onClick={submit}></PrimaryButton>
                </div>
            </div>
        </div>
    );
};

export default connect(null, mapDispatchToProps)(UserCreation);
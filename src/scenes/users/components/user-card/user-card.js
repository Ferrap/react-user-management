import "./user-card.css"
import man from "../../../../assets/images/man.png";
import woman from "../../../../assets/images/woman.png";
import PrimaryButton from "../../../../components/primary-button/primary-button";

const UserCard = ({ firstName, lastName, sexe, statut, onDelete }) => {
    return (
        <div className="card">
            <div className="second-avatar-circle">
                <div className="first-avatar-circle">
                    <img className="avatar" src={sexe === "female" ? woman : man}></img>
                </div>
            </div>
            <div className="name">
                <p className="first-name">{firstName}</p>
                <p className="last-name">{lastName}</p>
            </div>
            <p className="statut">{statut}</p>
            <div className="actions">
                <PrimaryButton text='Supprimer' color='#d7385e' onClick={onDelete}></PrimaryButton>
            </div>
        </div>
    )
}

export default UserCard;
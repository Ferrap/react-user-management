import './users.css'
import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import  { fetchUsers, deleteUser }  from '../../services/users.service'
import UserCard from './components/user-card/user-card';

const mapStateToProps = (state) => {
    return {
        users: state.users,
    }
}

const mapDispatchToProps = (dispatch) => {
    return  {
        deleteUser: id => dispatch(deleteUser(id)),
        fetchUser: () => dispatch(fetchUsers)
    }
}

const Users = (props) => {

    useEffect(() => {
        if (props.users.length === 0) {
            props.fetchUser();
        }
    });

   const onUserDelete = (id) => {
        props.deleteUser(id)
    }

    return (
        <div className='users-content'>
            <div className='filters'></div>
            <div className='users-list'>
                {props.users.map((data, index) => (
                    <div className='user'>
                        <UserCard key={index}
                            sexe={data.sexe}
                            firstName={data.firstName}
                            lastName={data.lastName}
                            statut={data.statut}
                            onDelete={() => onUserDelete(data.id)}
                        ></UserCard>
                    </div>
                ))}
            </div>
        </div>
    )
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
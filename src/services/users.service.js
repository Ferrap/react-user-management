import { db } from "../firebase";
import { ADD_USER, LOAD_USERS, DELETE_USER } from '../constants/action-types';

/**
 * Load the users from firebase
 * @param {*} dispatch 
 */
export async function fetchUsers(dispatch) {
    const response = await db.collection('users').get()
    const data = response.docs.map(doc => doc.data());
    dispatch({type : LOAD_USERS, payload: data})
}

/**
 * Delete an user from firebase
 * @param {*} id Id of the user to delete
 */
export function deleteUser(id) {
    return async function deleteUserThunk(dispatch) {
        try {
            await db.collection('users').doc(id.toString()).delete();
            console.log('Utilisateur supprimé');
            dispatch({ type: DELETE_USER, payload: id });
        } catch (error) {
            console.error(error);
        }
    }
  }

  /**
   * Add a new user to firebase
   * @param {*} user User to add.
   */
export function addUser(user) {
    return async function addUserThunk(dispath) {
        try {
            await db.collection('users').doc(user.id.toString())
                                        .set(user);
            console.log("Utilisateur ajouté");
            dispath({ type: ADD_USER, payload: user});
        } catch (error) {
            console.error(error);
        }
    }
}

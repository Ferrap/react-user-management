import  firebase from "firebase";


var config = {
    apiKey: "AIzaSyBy5uXlKonOLzXYMMQmHKJJSheFyYVODf0",
    authDomain: "user-management-eff1a.firebaseapp.com",
    databaseURL: "https://user-management-eff1a.firebaseio.com",
    projectId: "user-management-eff1a",
    storageBucket: "user-management-eff1a.appspot.com",
    messagingSenderId: "331483230548",
    appId: "1:331483230548:web:9ad4427c0de411fb63d8c1"
  };

const firebaseApp = firebase.initializeApp(config);
const db = firebaseApp.firestore();

export { db };
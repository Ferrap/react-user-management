import { ADD_USER, LOAD_USERS, DELETE_USER } from '../../constants/action-types';

const initialState = {
    users: []
};

const rootReducer = (state = initialState, action) => {
    if (action.type === ADD_USER) {
        return Object.assign({}, state, {
            users: state.users.concat(action.payload)
        });
    } else if (action.type === LOAD_USERS) {
        return Object.assign({},state, {
          users: state.users.concat(action.payload)
        });
    } else if (action.type === DELETE_USER) {
        return Object.assign({}, state, {
            users: initialState.users.filter(f => f.id !== action.payload)
        });
    }
    return state;
}

export default rootReducer;
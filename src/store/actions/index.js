import { ADD_USER, USERS_LOADED, USER_DELETED } from '../../constants/action-types'

export const addUser = (payload) => {
    return { type: ADD_USER, payload};
};

export const usersLoaded = (payload) => {
    return { type: USERS_LOADED, payload};
}

export const userDeleted = (payload) => {
    return { type: USER_DELETED, payload };
}

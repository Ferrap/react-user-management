import React from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";
import "./index.css";
import Users from "./scenes/users/users";
import Menu from "./components/menu/menu";
import store from "./store/index"
import UserCreation from "./scenes/user-creation/user-creation";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";



ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <div className="content">
          <Menu></Menu>
          <Switch>
            <Route path="/users" render={() => (<Users></Users>)} />
            <Route path="/user-creation" render={() => (<UserCreation></UserCreation>)} />
          </Switch>
        </div>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
